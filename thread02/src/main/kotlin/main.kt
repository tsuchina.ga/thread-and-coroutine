import kotlin.concurrent.thread

fun main(args: Array<String>) {

    val nodeTree = createNodeTree(10, 2)
    search(nodeTree)

}

fun search(node: Node) {

    if (node.children.isNotEmpty()) {
        node.children.forEach {
            thread {
                println("[%s]%dms start".format(Thread.currentThread(), System.currentTimeMillis()))
                search(it)
                Thread.sleep(1 * 1000)
                println("[%s]%dms end".format(Thread.currentThread(), System.currentTimeMillis()))
            }
        }
    }

}

fun createNodeTree(depth: Int, childNum: Int): Node {
    val node = Node()

    (1..childNum).forEach {
        if (depth == 0) {
            node.children += Node()
        } else {
            node.children += createNodeTree(depth - 1, childNum)
        }
    }

    return node
}

class Node {
    var children: List<Node> = arrayListOf()
}

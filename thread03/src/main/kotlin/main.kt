import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {

    val time = measureTimeMillis {
        val first = Thread.activeCount()

        (1..10).forEach { _ ->
            thread {
//                println("[%s]%dms start".format(Thread.currentThread(), System.currentTimeMillis()))

                var i: Long = 0
                (0..3_000_000_000).forEach { i += it }

//                println("[%s]%dms end".format(Thread.currentThread(), System.currentTimeMillis()))
            }
        }

        while (Thread.activeCount() > first) {}
    }

    println(time)
}

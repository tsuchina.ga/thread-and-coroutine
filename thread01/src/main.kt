import kotlin.concurrent.thread

fun main(args: Array<String>) {

    (0..10000).forEach {
        thread {
            println("[%s]%dms start: %d".format(Thread.currentThread(), System.currentTimeMillis(), it))
            Thread.sleep(3 * 1000)
            println("[%s]%dms  end: %d".format(Thread.currentThread(), System.currentTimeMillis(), it))
        }
    }

}

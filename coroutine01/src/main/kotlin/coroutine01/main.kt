package coroutine01

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

fun main(args: Array<String>) {

    (0..10000).forEach {
        launch {
            println("[%s]%dms start: %d".format(Thread.currentThread(), System.currentTimeMillis(), it))
            delay(3 * 1000)
            println("[%s]%dms  end: %d".format(Thread.currentThread(), System.currentTimeMillis(), it))
        }
    }

    Thread.sleep(5 * 1000)
}

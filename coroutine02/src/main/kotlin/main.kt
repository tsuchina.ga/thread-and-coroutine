import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

fun main(args: Array<String>) {

    val nodeTree = createNodeTree(10, 2)
    search(nodeTree)

    Thread.sleep(10 * 1000)
}

fun search(node: Node) {

    if (node.children.isNotEmpty()) {
        node.children.forEach {
            launch {
                println("[%s]%dms start".format(Thread.currentThread(), System.currentTimeMillis()))
                search(it)
                delay(1 * 1000)
                println("[%s]%dms end".format(Thread.currentThread(), System.currentTimeMillis()))
            }
        }
    }

}

fun createNodeTree(depth: Int, childNum: Int): Node {
    val node = Node()

    (1..childNum).forEach {
        if (depth == 0) {
            node.children += Node()
        } else {
            node.children += createNodeTree(depth - 1, childNum)
        }
    }

    return node
}

class Node {
    var children: List<Node> = arrayListOf()
}

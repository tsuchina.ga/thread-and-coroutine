import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val time = measureTimeMillis {
        var i: Long = 0

        (0..3_000_000_000).forEach { i += it }
    }

    println(time)
}
